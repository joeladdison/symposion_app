import bleach

tags = bleach.sanitizer.ALLOWED_TAGS[:]
tags.extend(['p', 'pre'])


def parse(text):
    scrubbed_text = bleach.clean(text, tags=tags)
    return scrubbed_text
