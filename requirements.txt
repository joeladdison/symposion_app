Django>=1.11
pinax-theme-bootstrap==7.3.0
pinax-eventlog==1.1.1
django-formset-js==0.5.0
dj-static==0.0.6
dj-database-url==0.4.2
pylibmc==1.5.1
django-debug-toolbar>=1.7
django-bootstrap-form

djangosaml2
django-gapc-storage

# database
mysqlclient>=1.3.3

# For testing
django-nose>=1.4.4
coverage==4.0.3


# Symposion reqs
django-appconf==1.0.1
django-model-utils>=3.0.0
django-reversion==1.10.1
django-sitetree>=1.8.0
django-taggit==0.18.0
django-timezone-field>=2.0
easy-thumbnails>=2.4.1
bleach
pytz==2015.7
django-ical==1.4


# registratoin reqs
django-nested-admin==2.2.6


# Registripe
django-countries>=4.3
pinax-stripe==3.2.1
requests>=2.11.1
stripe==1.38.0
