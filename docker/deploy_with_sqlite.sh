#!/bin/bash

/usr/local/bin/python /app/symposion_app/manage.py migrate
/usr/local/bin/python /app/symposion_app/manage.py loaddata /app/symposion_app/fixtures/conference.json /app/symposion_app/fixtures/proposal_base.json /app/symposion_app/fixtures/sites.json /app/symposion_app/fixtures/sitetree.json
/usr/local/bin/python /app/symposion_app/manage.py populate_inventory
exec /usr/local/bin/python /app/symposion_app/manage.py runserver 0.0.0.0:8000
